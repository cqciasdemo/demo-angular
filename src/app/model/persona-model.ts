export class PersonaModel {
  id: number = 0;
  nombre: string = '';
  primer_apellido: string = '';
  segundo_apellido: string = '';
  telefono: string = '';
  estatus: string = '';
  fecha_ins: string = '';
  fecha_upd: string = '';
}
