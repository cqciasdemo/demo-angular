import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonaModel } from '../model/persona-model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PersonaService {
  URL_API: string = 'http://localhost:9000/api/v1/demo';
  constructor(private httpClient: HttpClient) {}

  getPersons(): Observable<PersonaModel[]> {
    return this.httpClient
      .get<PersonaModel[]>(this.URL_API + '/list')
      .pipe(map((res) => res));
  }

  savePerson(request: any): Observable<PersonaModel[]> {
    return this.httpClient
      .post<any>(this.URL_API + '/save', request)
      .pipe(map((resp) => resp));
  }

  updatePerson(request: any): Observable<PersonaModel[]> {
    return this.httpClient
      .post<any>(this.URL_API + '/update', request)
      .pipe(map((resp) => resp));
  }
  deletePerson(id: number): Observable<any> {
    return this.httpClient
      .get<any>(this.URL_API + '/delete/' + id)
      .pipe(map((resp) => resp));
  }
}
