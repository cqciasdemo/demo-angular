import { PersonaService } from './../../service/persona.service';
import { AfterContentInit, Component } from '@angular/core';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs5';
import 'datatables.net-dt';
import { PersonaModel } from 'src/app/model/persona-model';
import { FormControl, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css'],
})
export class PersonaComponent implements AfterContentInit {
  listPersons: PersonaModel[] = [];
  formPerson: FormGroup = new FormGroup({});
  isUpdate: boolean = false;

  constructor(private personaService: PersonaService) {}

  ngAfterContentInit(): void {
    this.list();
    this.formPerson = new FormGroup({
      id: new FormControl(''),
      nombre: new FormControl(''),
      primer_apellido: new FormControl(''),
      segundo_apellido: new FormControl(''),
      telefono: new FormControl(''),
      estatus: new FormControl(''),
      fecha_ins: new FormControl(''),
      fecha_upd: new FormControl(''),
    });
    $(document).ready(function () {
      var table = $('#tablePersons').dataTable({
        language: {
          url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
        },
      });
      table.on('draw.dt', function () {
        if ($('#tablePersons_filter').length) {
          $('#tablePersons_filter').addClass('justify-content-end');
          $('#tablePersons_filter').addClass('d-flex');
          $('#tablePersons_paginate').addClass('justify-content-end');
          // $('#tablePersons_paginate').addClass('d-flex');
        }
      });
    });
  }

  onKeydown(event: KeyboardEvent) {
    const keyCode = event.keyCode;

    if ((keyCode < 48 || keyCode > 57) && keyCode !== 8 && keyCode !== 46) {
      event.preventDefault();
    }
  }

  list() {
    this.personaService.getPersons().subscribe((resp) => {
      if (resp) {
        this.listPersons = resp;
      }
    });
  }
  save() {
    const date = new Date();
    date.toISOString();
    const nombre = this.formPerson.controls['nombre'].value;
    const primerApellido = this.formPerson.controls['primer_apellido'].value;
    const segundoApellido = this.formPerson.controls['segundo_apellido'].value;
    const telefono = this.formPerson.controls['telefono'].value;

    if (
      nombre !== '' &&
      primerApellido !== '' &&
      segundoApellido !== '' &&
      telefono !== ''
    ) {
      this.formPerson.controls['estatus'].setValue('1');
      this.formPerson.controls['fecha_ins'].setValue(date);

      this.personaService
        .savePerson(this.formPerson.value)
        .subscribe((resp) => {
          if (resp) {
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Guardado',
              showConfirmButton: false,
              timer: 1000,
            }).then(() => {
              this.list();
              this.formPerson.reset();
              this.closeModal();
            });
          }
        });
    } else {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Campos vacios,complete el formulario',
        showConfirmButton: false,
        timer: 1000,
      });
    }
  }

  update() {
    const date = new Date();
    date.toISOString();
    // this.formPerson.controls['estatus'].setValue('1');
    this.formPerson.controls['fecha_upd'].setValue(date);
    this.personaService
      .updatePerson(this.formPerson.value)
      .subscribe((resp) => {
        if (resp) {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Guardado',
            showConfirmButton: false,
            timer: 1000,
          }).then(() => {
            this.list();
            this.formPerson.reset();
            this.closeModal();
          });
          this.list();

          this.formPerson.reset();
        }
      });
    this.closeModal();
  }

  delete(id: any) {
    Swal.fire({
      title: '¿Estás seguro de eliminar este elemento?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: 'Guardar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.personaService.deletePerson(id).subscribe((resp) => {
          if (resp) {
            this.list();
          }
        });
        Swal.fire('Eliminado!', '', 'success');
      } else if (result.isDenied) {
        Swal.fire('Proceso cancelado', '', 'info');
      }
    });
  }

  closeModal() {
    this.isUpdate = false;
    this.formPerson.reset();
    $('#voltaic_holder').css({ display: 'none' });
    $;
  }

  selectItem(item: any) {
    this.isUpdate = true;

    this.formPerson.controls['fecha_ins'].disable();
    this.formPerson.controls['fecha_upd'].disable();

    this.formPerson.controls['id'].setValue(item.id);
    this.formPerson.controls['nombre'].setValue(item.nombre);
    this.formPerson.controls['primer_apellido'].setValue(item.primer_apellido);
    this.formPerson.controls['segundo_apellido'].setValue(
      item.segundo_apellido
    );
    this.formPerson.controls['telefono'].setValue(item.telefono);
    this.formPerson.controls['estatus'].setValue(item.estatus);
    this.formPerson.controls['fecha_ins'].setValue(item.fecha_ins);
    this.formPerson.controls['fecha_upd'].setValue(item.fecha_upd);
  }
}
